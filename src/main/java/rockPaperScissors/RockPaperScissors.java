package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        //variabel n for while loop, round counter teller antall runder :) 
        int n = 1;
        int round_counter = 1;

        //første while loop gjør slik at den kun vil kalle etter ny runde etter en gjennomfort runde med riktig input
        while (n == 1){
            //variabel t for while loop
            int t = 1;
            System.out.printf("Let's play round %d\n", round_counter);
            //while loop der programet kjører steinsakks papir helt til en runde er gjennomført
            while (t == 1) {
                //System.out.printf("Let's play round %d\n", round_counter);
                String user = readInput("Your choice (Rock/Paper/Scissors)?");

                //dette er for å genere en random in mellom 0-2, for å hente ut en random index fra listen
                Random rand = new Random();
                int upperbound = 3;
                int int_random = rand.nextInt(upperbound);
                String computer = rpsChoices.get(int_random);

                //først en test for å sjekke input
                //så en test for å se om input og computer har valgt likt
                //mange if tester for å teste alle vinner mulighetene til user, dersom user ikke vinner så vinner computer.
                if (rpsChoices.contains(user) == false) {
                    System.out.printf("I do not understand %s. Could you try again?\n", user);
                    continue;
                }
                else if (user.equals(computer)) {
                    System.out.printf("Human chose %s, computer chose %s. It's a tie!\n",user,computer);
                }
                else if (user.equals("paper") && computer.equals("rock")) {                  
                    System.out.printf("Human chose %s, computer chose %s. User wins!\n",user,computer);
                    humanScore += 1;    
                }
                else if (user.equals("rock") && computer.equals("scissors")) {
                    System.out.printf("Human chose %s, computer chose %s. User wins!\n",user,computer);
                    humanScore += 1;
                }
                else if (user.equals("scissors") && computer.equals("paper")) {  
                    System.out.printf("Human chose %s, computer chose %s. User wins!\n",user,computer);
                    humanScore += 1;     
                }
                else {
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n",user,computer);
                    computerScore+=1;
                }
                
                System.out.printf("Score: human %s, computer %s\n",humanScore,computerScore);

                //if tester for om bruker vil fortsette eller ikke
                
                while (t==1){
                    String svar = readInput("Do you wish to continue playing? (y/n)?");
                    if (svar.equals("y")) {
                        round_counter += 1;
                        t=0;
                        
                    }
                    else if (svar.equals("n")) {
                        System.out.println("Bye bye :)");
                        t=0;
                        n=0;
                        
                    }
                    else {
                        System.out.println("Venligst skriv y eller n :)");
                    }
                }

                
            }
        }
        // TODO: Implement Rock Paper Scissors

        
    }
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
